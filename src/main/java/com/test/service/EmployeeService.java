package com.test.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.data.EmployeeRepository;
import com.test.model.Employee;

@Service
public class EmployeeService {

	// @Autowired annotation provides the automatic dependency injection.
		@Autowired
		EmployeeRepository repository;

		// Save student entity in the h2 database.
		public Employee save(final Employee employee) {
			return repository.save(employee);
		}

		// Get all students from the h2 database.
		public List<Employee> getAll() {
			final List<Employee> employees = new ArrayList<>();
			repository.findAll().forEach(e -> employees.add(e));
			//sort on employee name
			List<Employee> employeeSOrt = employees.stream().sorted(Comparator.comparing(Employee::getName)).collect(Collectors.toList());
			return employeeSOrt;
		}
}
