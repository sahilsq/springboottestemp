<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<body>
<div style="border:thin dotted black; padding:3mm; line-height:1.6;">
    <div align=center>
    <h1>Add new employee</h1>
        
    <form:form modelAttribute="form">
        <form:errors path="" element="div" />
        <table>
        	<tr>
            <td><form:label path="name">Name</form:label></td>
            <td><form:input path="name" /></td>
            <td><form:errors path="name" /></td>
        </tr>
        <tr>
        	<td><form:label path="age">Age</form:label>
            <td><form:input path="age" /></td>
            <td><form:errors path="age" /></td>
        </tr>
        <tr>
        	<td><form:label path="emailAddress">Email Address</form:label>
            <td><form:input path="emailAddress" />
            <td><form:errors path="emailAddress" />
        </tr>
        <tr>
        	<td><form:label path="dept">Department</form:label>
            <td><form:input path="dept" />
            <td><form:errors path="dept" />
        </tr><tr></tr>
        <tr>
            <td colspan=3 align=center><input type="submit" value="Add" /></td>
        </tr>	
        	
        </table>
        
    </form:form>
    </div>
    </div>
</body>
</html>
