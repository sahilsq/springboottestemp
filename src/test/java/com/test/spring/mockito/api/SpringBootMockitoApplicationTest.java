package com.test.spring.mockito.api;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runner.Runner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.test.data.EmployeeRepository;
import com.test.model.Employee;
import com.test.service.EmployeeService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootMockitoApplicationTest {

	@Autowired
	private EmployeeService service;
	
	@MockBean
	private EmployeeRepository repos;
	
	@Test
	public void saveTest() {
		Employee emp = new Employee(1,"name",12,"mail@gmail.com","deptt");
		when(repos.save(emp)).thenReturn(emp);
		assertEquals(emp, service.save(emp));
	}

	@Test
	public void getAllTest(){
		when(repos.findAll()).thenReturn(Stream.of(new Employee(1,"namee",11,"mail1@mail.com","dep1"),new Employee(2,"namee2",21,"mail@mail.com","dep2")).collect(Collectors.toList()));
		assertEquals(2, service.getAll().size());
	}
	
}
