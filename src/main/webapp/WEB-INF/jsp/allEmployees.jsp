<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



<body>

 <div align="center">
 <div style="border:thin dotted black; padding:3mm; line-height:1.6;">
 	<div><h1>Employees Details</h1></div>
 	<br/>
	<a align="right" href="/addNewEmployee.html" >Add an employee </a>
	<br/>
	<table cellpadding=5 border=1 border-width=1>
		<thead>
			<tr>
				<th>Id</th>
				<th>Employee Name</th>
				<th>Age</th>
				<th>Email Address</th>
				<th>Department</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${employees}" var="employee">
			<tr>
					<td>${employee.id}	</td>
					<td>${employee.name}	</td>
					<td>${employee.age}	</td>
					<td>${employee.emailAddress}	</td>
					<td>${employee.dept}	</td>
				
			</tr>
			</c:forEach>
		</tbody>
	</table>
	</div>
 </div>
 
</body>

<%-- <ul>
<c:forEach items="${employees}" var="employee">
   <li>${employee}</li>
</c:forEach>
</ul> --%>