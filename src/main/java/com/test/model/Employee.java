package com.test.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Employee {
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private long id;
	private String name;
	private Integer age;
	private String emailAddress;
	private String dept;
	
	public Employee() {};
	
	
	public Employee(long id, String name, int age, String emailAddress, String dept) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.emailAddress = emailAddress;
		this.dept = dept;
	}
	 

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", age=" + age +   ", emailAddress=" + emailAddress + ", dept=" + dept + "]";
	}

}
